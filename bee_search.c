/*
 * bee_search.c
 *
 * Implementation of Bee algorithm with
 * couple of test functions.
 *
 * 12 April 2014  Filip Tehlar
 */
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

/* flag used to indicate what search neighbourhood method
 * is used; comment out following define statement to use
 * constant selection of M fittest bees else probability
 * based method is used */
#define CONST_SELECT

/* Control parameters of bee algorithm */
#define N       50 /* population */
#define M       15  /* site number */
#define E       3   /* number of elite sites */
#define NGH     10  /* initial patch size */
#define NEP     10  /* number of bees around elite points */
#define NSP     5  /* number of bees around selected points */

/* parameters of optimized problem */
#define CYCLE_COUNT 1000
#define D           10      /* dimension of optimizied problem */
#define LB          -500    /* lower bound */
#define UB          500     /* upper bound */

#define MUT_FAC     0.15     /* factor of mutation */

typedef double (*fit_fcn)(double sol[D]);
typedef struct bee_s {
    double sol[D];
    double val;
} bee_t;

/* FORWARD DEFINITIONS */
double rnd();       /* random number from interval (0,1> */
void init_bee(int index); /* initialize bee position at index */
void init_pop();
int cmpbee(const void* b1, const void* b2);
void eval_bees();
void recruit_bees();
void recruit_scouts();
void memorize_best();
double fittness(double val);

/* OPTIMIZATION PROBLEMS */
double schwef(double sol[D]);
double sphere(double sol[D]);

/* GLOBAL VARIABLES */
fit_fcn function = schwef;
bee_t population[N];
bee_t best;
unsigned visited_points = 0;

/* BEE ALGORITHM */
int
main ()
{
    int i;

    srand(time(NULL));
    init_pop();
    for (i=0; i<CYCLE_COUNT; i++) {
        eval_bees();
        qsort(population, N, sizeof(bee_t), cmpbee);
        recruit_bees();
        recruit_scouts();
        memorize_best();
    }

    for (i=0; i<D; i++) {
        printf("sol[%d]=%f\n", i, best.sol[i]);
    }
    printf("visited points: %u\n", visited_points);
    printf("f(best)=%f\n", best.val);

    return 0;
}

double
fitness (double val)
{
    if (val > 0) {
        return (1/(val+1));
    }
    return (1+fabs(val));
}

void
recruit_bees ()
{
    int i, j, k;
    int recruit_num;
    bee_t tmp_bee;
    int dir;
    int ngh;

#ifdef CONST_SELECT
    /*for each site recruit some bees */
    for (i=0; i<M; i++) {
        /* for elite sites recruit more bees  */
        if (i < E) {
            recruit_num = NEP;
        } else {
            recruit_num = NSP;
        }

        /* recruit bees for a site, move the fittest bee to the population */
        for (j=0; j<recruit_num; j++) {
            ngh = rnd()*N;
            while (ngh == i) {
                ngh = rnd()*N;
            }
            memcpy(&tmp_bee, &population[i], sizeof(tmp_bee));
            for (k=0; k<D; k++) {
                if (rnd() < MUT_FAC) {
                    tmp_bee.sol[k] = population[i].sol[k] + (population[i].sol[k] - population[ngh].sol[k])*(rnd()*NGH - NGH/2);
                    if (tmp_bee.sol[k] > UB) tmp_bee.sol[k] = UB;
                    if (tmp_bee.sol[k] < LB) tmp_bee.sol[k] = LB;
                }
            }

            tmp_bee.val = function(tmp_bee.sol);
            if (tmp_bee.val < population[i].val) {
                memcpy(&population[i], &tmp_bee, sizeof(bee_t));
            }
        }
    }
#else
    /* determine the probability of a bee to be chosen
     * for neighbourhood search */
    i = 0;
    double prob[N];
    double total_fit = 0;
    for (i=0; i<N; i++) {
        total_fit += fitness(population[i].val);
    }
    for (i=0; i<N; i++) {
        prob[i] = fitness(population[i].val) / total_fit;
    }

    int t = 0;
    i = 0;
    while (t < M) {
        if (rnd() < prob[i]) {
            t++;
            recruit_num = NSP;
            for (j=0; j<recruit_num; j++) {
                ngh = rnd()*N;
                while (ngh == i) {
                    ngh = rnd()*N;
                }
                memcpy(&tmp_bee, &population[i], sizeof(tmp_bee));
                for (k=0; k<D; k++) {
                    if (rnd() < MUT_FAC) {
                        tmp_bee.sol[k] = population[i].sol[k] +
							(population[i].sol[k] - population[ngh].sol[k])*(rnd()*NGH - NGH/2);
                        if (tmp_bee.sol[k] > UB) tmp_bee.sol[k] = UB;
                        if (tmp_bee.sol[k] < LB) tmp_bee.sol[k] = LB;
                    }
                }

                tmp_bee.val = function(tmp_bee.sol);
                if (tmp_bee.val < population[i].val) {
                    memcpy(&population[i], &tmp_bee, sizeof(bee_t));
                }
            }
        }

        i++;
        if (i == N) {
            i = 0;
        }
    }
#endif
}

void
recruit_scouts ()
{
    int i;
    for (i=M; i<N; i++) {
        init_bee(i);
    }
}

void
memorize_best ()
{
    int i;
    best.val = population[0].val;
    for (i=1; i<N; i++) {
        if (population[i].val < best.val) {
            memcpy(&best, &population[i], sizeof(bee_t));
        }
    }
}

void
eval_bees ()
{
    int i;
    for (i=0; i<N; i++) {
        population[i].val = function(population[i].sol);
    }
}

int
cmpbee (const void* b1, const void* b2)
{
    const bee_t* bee1 = (const bee_t*)b1;
    const bee_t* bee2 = (const bee_t*)b2;

    if (bee1->val == bee2->val) {
        return 0;
    }
    if (bee1->val < bee2->val) {
        return -1;
    }
    return 1;
}

void
init_pop ()
{
    int i;
    for (i=0; i<N; i++) {
        init_bee(i);
    }
}

void
init_bee (int index)
{
    int j;
    for (j=0; j<D; j++) {
        population[index].sol[j] = rnd()*(UB-LB)+LB;
    }
}

double
rnd ()
{
    return ((double)rand()/((double)(RAND_MAX)+(double)(1)));
}

double
schwef (double sol[D])
{
    int i;
    double res = 0;
    visited_points++;
    for (i=0; i<D; i++) {
        res = res - sol[i] * sin(sqrt(fabs(sol[i])));
    }
    return res;
}

double
sphere (double sol[D])
{
    int i;
    double res = 0;

    visited_points++;
    for (i=0; i<D; i++) {
        res += sol[i]*sol[i];
    }
    return res;
}
